const {
    users
} = require("../models");

class usersRepository {
    static async getByEmail({
        email
    }) {
        const getUsersByEmail = await users.findOne({
            where: {
                email
            }
        })
        return getUsersByEmail;
    }

    static async register({
        username,
        email,
        password,
        role,
    }) {
        const registered_Users = users.create({
            username,
            email,
            password,
            role,
        })

        return registered_Users;
    }

    static async create({ 
        name, 
        email, 
        password, 
        role 
    }) { 
        const createdUser = users.create({ 
            name, 
            email, 
            password, 
            role, 
        }); 
 
        return createdUser; 
    }
}

module.exports = usersRepository;



// module.exports = {
//     getByEmail : ({email}) => users.findOne({ where: { email } }),
//     register : (data) => users.create(data)
// }