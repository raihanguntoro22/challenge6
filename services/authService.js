const usersRepository = require("../repositories/usersRepository");
const bycrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
    JWT
} = require("../lib/const");
const { OAuth2Client } = require("google-auth-library");
const SALT_ROUND = 10;           //tingkat keruwetan encrypt, semakin banyak semakin ruwet 

class authService {
    static async register({
        username,
        email,
        password,
        role,
    }) {
        try {
            if (!email) {       //VALIDASI UNTUK MENGECEK
                return {
                    status: false,
                    code_status: 400,
                    message: "email wajib diisi",
                    data: {
                        registered_Users: null,
                    }
                }
            };

            if (!username) {
                return {
                    status: false,
                    code_status: 400,
                    message: "name wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!role) {
                return {
                    status: false,
                    code_status: 400,
                    message: "role wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!password) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            } else if (password.length < 8) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password minimal 8 karakter",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            const getByEmail = await usersRepository.getByEmail({
                email
            });

            if (getByEmail) {
                return {
                    status: false,
                    code_status: 400,
                    message: "email sudah terdaftar",
                    data: {
                        registered_Users: null,
                    },
                };
            } else {
                const hashingPassword = await bycrypt.hash(password, SALT_ROUND);
                const regsiteredUsers = await usersRepository.register({
                    username,
                    email,
                    password: hashingPassword,
                    role,
                });

                return {
                    status: true,
                    code_status: 201,
                    message: "Users berhasil registrasi",
                    data: {
                        registered_Users: regsiteredUsers,
                    },
                };
            }


        } catch (err){
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    registered_Users: null,
                },
            };
        }

    }

    static async loginGoogle({ 
        google_credential: googleCredential 
    }) { 
        try { 
            const client = new OAuth2Client("744631181864-jmm2c7mvco93f6fl0co1lii4s1rv75fv.apps.googleusercontent.com"); 
 
            // Get google user credential 
            const userInfo = await client.verifyIdToken({ 
                idToken: googleCredential, 
                audience: "744631181864-jmm2c7mvco93f6fl0co1lii4s1rv75fv.apps.googleusercontent.com", 
            }); 
 
            const { 
                email, 
                name 
            } = userInfo.payload; 
 
            const getUserByEmail = await usersRepository.getByEmail({ 
                email 
            }); 
 
            if (!getUserByEmail) { 
                await usersRepository.create({ 
                    name, 
                    email, 
                    role: "member", 
                }); 
            } 
 
            const token = jwt.sign({ 
                    id: getUserByEmail.id, 
                    email: getUserByEmail.email, 
                }, 
                JWT.SECRET, { 
                    expiresIn: JWT.EXPIRED, 
                } 
            ); 
 
            return { 
                status: true, 
                status_code: 200, 
                message: "User berhasil login", 
                data: { 
                    token, 
                }, 
            }; 
        } catch (err) { 
            console.log(err); 
            return { 
                status: false, 
                status_code: 500, 
                message: err.message, 
                data: { 
                    registered_user: null, 
                }, 
            }; 
        } 
    }

    static async login({
        email,
        password
    }){
        try{
            if (!email) {
                return {
                    status: false,
                    code_status: 400,
                    message: "email wajib diisi",
                    data: {
                        registered_shops: null,
                    }
                }
            };
    
            if (!password) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password wajib diisi",
                    data: {
                        registered_shops: null,
                    },
                };
            } else if (password.length < 8) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password harus minimal 8 karakter",
                    data: {
                        registered_shops: null,
                    },
                };
            }
    
            const getUsers = await usersRepository.getByEmail({
                email
            });
    
            if(!getUsers){      //mengecek email sudah terdaftar apa belom
                return{
                    status: false,
                    code_status: 400,
                    message: "email belum terdaftar!",
                    data:{
                        registered_Users: null,
                    },
                };
            } else{
                const passwordMatching = await bycrypt.compare(password, getUsers.password);
    
                if (passwordMatching){
                    const token = jwt.sign({
                        id: getUsers.id,
                        email: getUsers.email,
                    }, JWT.SECRET, {
                        expiresIn: JWT.EXPIRED,
                    });
    
                    return {
                        status: true,
                        code_status: 200,
                        message: "anda berhasil login",
                        data: {
                            token,
                        },
                    };
                } else {
                    return {
                        status: false,
                        code_status: 400,
                        message: "password anda salah, mohon isi ulang",
                        data: {
                            registered_shops: null,
                        },
                    };
                }
            }
        }catch (err){
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    registered_Users: null,
                },
            };
        }
    }
}

module.exports = authService;
