require('dotenv').config()
const express = require("express");
const app = express();
const cors = require ('cors')
const port = process.env.PORT || 3500
const bodyParser = require("body-parser");
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
// import controllers
const authController = require("./controllers/authController");
const carsController = require("./controllers/carsController");
// import middleware
const middlewares = require("./middlewares/auth");
//import Swagger
const YAML = require("yamljs")
const swaggerUI = require("swagger-ui-express")
const apiDocs = YAML.load("./api-docs.yaml")

// define routes auth
// register member
app.post("/auth/register", authController.register);

// register admin  membuat admin yg  bisa membuat hanya superadmin
app.post("/auth/register/admin", middlewares.authenticate, middlewares.isSuperAdmin, authController.register);

app.post("/auth/login", authController.login);
app.get("/auth/me", middlewares.authenticate, authController.currentUser);
app.post("/auth/login-google", authController.loginGoogle)

// define routes CRUD
app.get("/cars", middlewares.authenticate, middlewares.roles, carsController.getCars);
app.post("/cars/create", middlewares.authenticate, middlewares.roles, carsController.create);
app.put("/cars/update/:id", middlewares.authenticate, middlewares.roles, carsController.update);
app.delete("/cars/delete/:id", middlewares.authenticate, middlewares.roles, carsController.deleted);
app.get("/cars/filtered?", carsController.filtered )


// API Documentation
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(apiDocs));


app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});
